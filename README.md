# docker-jdk8-mvn-sbt

> - some tools for my side projects
> - largely inspired by https://bitbucket.org/iflavours/mvn-sbt-openjdk-8-alpine/src/master/

## How to

### Build the image

```shell
IMAGE_NAME="mvn-sbt-jdk-8"
echo "IMAGE_NAME: ${IMAGE_NAME}"

docker build --no-cache -t ${IMAGE_NAME}  .
```

### Publish the image to Docker hub

```shell
IMAGE_NAME="mvn-sbt-jdk-8"
echo "IMAGE_NAME: ${IMAGE_NAME}"
TAG="0.0.1"

docker tag ${IMAGE_NAME} ${DOCKER_HANDLE}/${IMAGE_NAME}:${TAG}

echo ${DOCKER_PWD} | docker login --username ${DOCKER_HANDLE} --password-stdin

docker push ${DOCKER_HANDLE}/${IMAGE_NAME}:${TAG}
```

### Use the image ith GitLab CI

```yaml
image: k33g/mvn-sbt-openjdk-8-alpine:0.0.1

stages:
  - 🎃scala
  - 🎅maven

run-hello:
  stage: 🎃scala
  only:
    - master
  script: |
    cd hello
    sbt run

build-yo:
  stage: 🎅maven
  only:
    - master
  script: |
    cd yo
    mvn clean package
    ls target/*.jar
```


## Disclaimer :wave: :warning:

These scripts are provided for educational purposes. This project is subject to an opensource license (feel free to fork it, and make it better). You can modify the scripts according to your needs. This project is not part of the GitLab support. However, if you need help do not hesitate to create an issue in the associated project, or better to propose a Merge Request.
:warning: Before using these scripts in production, check the consistency of the results on test instances, and of course make backups.